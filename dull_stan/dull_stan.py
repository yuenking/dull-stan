import numpy as np

import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html


def _create_dropdown_options(flatnames):
    return [{'label': f, 'value': i} for (i, f) in enumerate(flatnames)]


def _generate_chain_data(stan_fit, selected_dropdown_value):
    chains = stan_fit.extract(permuted=False)[:, :, selected_dropdown_value]
    return [{'x': np.arange(len(chain)), 'y': chain, 'type': 'lines',
             'name': 'chain {}'.format(i)} for (i, chain) in enumerate(chains.T)]


def _get_sim_values(stan_fit, selected_dropdown_value):
    flatname = stan_fit.flatnames[selected_dropdown_value]
    if "[" in flatname:
        param, index_str = flatname.split("[")
        index = int(index_str.rstrip("]"))
        return stan_fit.extract()[param][:, index]
    else:
        return stan_fit.extract()[flatname]


def _generate_hist_data(stan_fit, selected_dropdown_value):
    sim_values = _get_sim_values(stan_fit, selected_dropdown_value)
    xbins = {'start': np.min(sim_values), 'end': np.max(sim_values),
             'size': round((np.max(sim_values) - np.min(sim_values)) / 20, 1)}
    hist_values = [{'x': sim_values, 'type': 'histogram', 'histnorm': 'probability', 'name': 'all', 'opacity': 0.9,
                    'xbins': xbins}]

    chains = stan_fit.extract(permuted=False)[:, :, selected_dropdown_value]
    chain_hist_values = [{'x': chain, 'type': 'histogram', 'histnorm': 'probability', 'opacity': 0.5,
                          'xbins': xbins, 'visible': 'legendonly',
                          'name': 'chain {}'.format(i)} for (i, chain) in enumerate(chains.T)]
    return hist_values + chain_hist_values


def launch_dull_stan(stan_fit):
    """starts a dash server to visualize the stan fit object"""
    flatnames = stan_fit.flatnames

    app = dash.Dash()

    app.layout = html.Div([
        html.H1('Chains'),
        dcc.Dropdown(
            id='my-dropdown',
            options=_create_dropdown_options(flatnames),
            value=0
        ),
        dcc.Graph(id='my-graph'),
        dcc.Graph(id='my-histogram')
    ])

    @app.callback(Output('my-graph', 'figure'), [Input('my-dropdown', 'value')])
    def update_graph(selected_dropdown_value):
        return {
            'data': _generate_chain_data(stan_fit, selected_dropdown_value)
        }

    @app.callback(Output('my-histogram', 'figure'), [Input('my-dropdown', 'value')])
    def update_histogram(selected_dropdown_value):
        return {
            'data': _generate_hist_data(stan_fit, selected_dropdown_value),
            'layout': {'barmode': 'overlay'}
        }

    app.run_server()
