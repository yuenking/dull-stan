# dull-stan

This is a simple alternative library for plotting chains from
[Stan](http://mc-stan.org/) written in python.

[ShinyStan](http://mc-stan.org/users/interfaces/shinystan) is a very
useful tool for visualizing chains and diagnosing the model. However, it
is only available for R; and it is rather annoying to use pystan with
it.

This package is built on [plotly dash](https://plot.ly/products/dash/).
It will start a server on 8050 with the function launch_dull_stan().
